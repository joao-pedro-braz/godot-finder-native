use std::collections::hash_map::DefaultHasher;
use std::collections::HashMap;
use std::ffi::OsStr;
use std::fs;
use std::hash::{Hash, Hasher};

use gdnative::prelude::*;
use walkdir::{DirEntry, WalkDir};

use crate::script_parser::Script;

pub fn init(handle: &InitHandle) {
    handle.add_tool_class::<FileTree>();
}

#[derive(NativeClass, Default, Debug, ToVariant, FromVariant)]
#[inherit(Reference)]
pub struct FileTree(pub HashMap<String, Option<Script>>);

impl FileTree {
    pub fn try_insert_entry(&mut self, entry: DirEntry, smart_suggestions: bool, cache_path: &str) {
        if ResourceLoader::godot_singleton().exists(entry.path().to_str().unwrap(), "") {
            match entry.path().extension().and_then(OsStr::to_str) {
                Some("gd") => {
                    self.0.insert(
                        entry.path().to_str().unwrap().to_string(),
                        Some(self.get_script(entry, smart_suggestions, cache_path)),
                    );
                }
                _ => {
                    self.0
                        .insert(entry.path().to_str().unwrap().to_string(), None);
                }
            }
        }
    }

    fn get_script(&self, entry: DirEntry, smart_suggestions: bool, cache_path: &str) -> Script {
        let mut script = Script::new();

        if smart_suggestions {
            fs::create_dir_all(cache_path).unwrap();

            let script_content = fs::read_to_string(entry.path()).expect("Failed to open file");
            let mut script_content_hasher = DefaultHasher::new();
            let mut script_path_hasher = DefaultHasher::new();

            script_content.hash(&mut script_content_hasher);
            entry.path().hash(&mut script_path_hasher);
            match fs::read(format!(
                "{}/{}_{}",
                cache_path,
                script_path_hasher.finish(),
                script_content_hasher.finish()
            )) {
                Ok(content) => {
                    let (decoded, _): (Script, usize) =
                        bincode::decode_from_slice(&content, bincode::config::standard()).unwrap();
                    script = decoded;
                }
                Err(_) => {
                    // Removes the older versions of the cache
                    for file in WalkDir::new(cache_path)
                        .into_iter()
                        .filter_map(|entry| entry.ok())
                        .filter(|entry| {
                            entry
                                .path()
                                .starts_with(script_path_hasher.finish().to_string())
                        })
                    {
                        fs::remove_file(file.path()).unwrap();
                    }

                    script.parse_script(script_content);
                    let encoded =
                        bincode::encode_to_vec(&script, bincode::config::standard()).unwrap();
                    fs::write(
                        format!("{}/{}", cache_path, script_content_hasher.finish()),
                        encoded,
                    )
                    .unwrap();
                }
            }
        }

        script
    }
}

#[methods]
impl FileTree {
    fn new(_: TRef<Reference>) -> Self {
        FileTree::default()
    }
}
