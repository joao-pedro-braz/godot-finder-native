use gdnative::prelude::*;
use tokio::runtime::{Builder, Runtime};

use crate::EXECUTOR;

pub fn init(handle: &InitHandle) {
    handle.add_tool_class::<AsyncExecutorDriver>();
}

#[derive(NativeClass)]
#[inherit(Node)]
struct AsyncExecutorDriver {
    runtime: Runtime,
}

impl AsyncExecutorDriver {
    fn new(_owner: &Node) -> Self {
        AsyncExecutorDriver {
            runtime: Builder::new_current_thread().build().unwrap(),
        }
    }
}

#[methods]
impl AsyncExecutorDriver {
    #[method]
    fn _process(&self, _delta: f64) {
        EXECUTOR.with(|e| {
            self.runtime
                .block_on(async {
                    e.run_until(async { tokio::task::spawn_local(async {}).await })
                        .await
                })
                .unwrap()
        })
    }
}
