use std::cmp::Ordering;

use walkdir::DirEntry;

pub fn is_valid_entry(entry: &DirEntry, excluded_paths: &Vec<String>) -> bool {
    entry
        .file_name()
        .to_str()
        .map(|s| s.starts_with("."))
        .unwrap_or(false)
        && !excluded_paths
            .into_iter()
            .any(|path| entry.path().starts_with(path))
        && !entry.path().starts_with(".")
}

pub fn float_sorter(a: f32, b: f32) -> Ordering {
    if a > b {
        Ordering::Greater
    } else if a < b {
        Ordering::Less
    } else {
        Ordering::Equal
    }
}
