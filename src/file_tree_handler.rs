use futures::channel::oneshot::channel;
use std::collections::HashMap;
use std::{fs, thread};

use gdnative::prelude::*;
use gdnative::tasks::{Async, AsyncMethod, Spawner};
use walkdir::WalkDir;

use crate::file_tree::FileTree;
use crate::fuzzy_matching::{classify_file, classify_script_properties};
use crate::helpers::{float_sorter, is_valid_entry};
use crate::script_parser::ScriptProperty;

pub fn init(handle: &InitHandle) {
    handle.add_tool_class::<FileTreeHandler>();
}

#[derive(NativeClass)]
#[inherit(Reference)]
#[register_with(register_methods)]
pub struct FileTreeHandler;

#[methods]
impl FileTreeHandler {
    fn new(_: TRef<Reference>) -> Self {
        FileTreeHandler
    }
}

struct BuildFileTreeFn;

impl AsyncMethod<FileTreeHandler> for BuildFileTreeFn {
    fn spawn_with(&self, spawner: Spawner<'_, FileTreeHandler>) {
        spawner.spawn(|_ctx, _this, mut args| {
            let base_dir_path = args.read::<String>().get().unwrap();
            let cache_dir_path = args.read::<String>().get().unwrap();
            let excluded_paths: Vec<String> = args
                .read::<PoolArray<GodotString>>()
                .get()
                .unwrap()
                .to_vec()
                .into_iter()
                .map(|str| str.to_string())
                .collect();
            let smart_suggestions = args.read::<bool>().get().unwrap();
            let invalidate_script_cache = args.read::<bool>().get().unwrap();

            let base_dir = WalkDir::new(base_dir_path.to_string());

            let (sender, receiver) = channel::<Variant>();
            thread::spawn(move || {
                if invalidate_script_cache {
                    let _ = fs::remove_dir_all(cache_dir_path.as_str());
                }

                let mut file_tree = FileTree::default();

                for entry in base_dir
                    .into_iter()
                    .filter_entry(|entry| !is_valid_entry(entry, &excluded_paths))
                    .filter_map(|entry| entry.ok())
                {
                    file_tree.try_insert_entry(entry, smart_suggestions, cache_dir_path.as_str());
                }

                sender.send(file_tree.to_variant())
            });

            async move { receiver.await.unwrap() }
        })
    }
}

struct FilterFileTreeFn;

impl AsyncMethod<FileTreeHandler> for FilterFileTreeFn {
    fn spawn_with(&self, spawner: Spawner<'_, FileTreeHandler>) {
        spawner.spawn(|_ctx, _this, mut args| {
            let file_tree = args.read::<FileTree>().get().unwrap();
            let search = args.read::<String>().get().unwrap();
            let max_files = args.read::<usize>().get().unwrap_or(50);

            let (sender, receiver) = channel::<Variant>();
            thread::spawn(move || {
                let mut files: HashMap<String, Option<Vec<ScriptProperty>>> = HashMap::new();
                let mut files_scores: HashMap<String, f32> = HashMap::new();

                for (file_path, maybe_script) in file_tree.0.into_iter() {
                    let score = classify_file(file_path.as_str(), search.as_str());
                    if score < 1.0 {
                        files.insert(file_path.clone(), None);
                        files_scores.insert(file_path.clone(), score);
                    }

                    if let Some(properties) = maybe_script {
                        let (properties_scores, min_property_score) =
                            classify_script_properties(properties.enumerate(), search.as_str());

                        if score < 1.0 || properties_scores.len() > 0 {
                            let mut filtered_properties =
                                properties_scores.iter().collect::<Vec<_>>();

                            filtered_properties.sort_by(|a, b| float_sorter(*a.1, *b.1));

                            files.insert(
                                file_path.clone(),
                                Some(
                                    filtered_properties
                                        .into_iter()
                                        .map(|(property, _)| property.clone())
                                        .collect(),
                                ),
                            );
                            files_scores.insert(file_path.clone(), score.min(min_property_score));
                        }
                    }
                }

                let mut files_scores_vec: Vec<_> = files_scores.iter().collect();
                files_scores_vec.sort_by(|a, b| float_sorter(*a.1, *b.1));

                let mut sorted_files: Vec<(String, Vec<ScriptProperty>)> = Vec::new();
                for (file, _) in files_scores_vec {
                    let properties = if let Some(properties) = files.get(&file.clone()).unwrap() {
                        properties.clone()
                    } else {
                        Vec::new()
                    };
                    sorted_files.push((file.clone(), properties));

                    if sorted_files.len() >= max_files {
                        break;
                    }
                }

                sender.send(sorted_files.to_variant())
            });

            async move { receiver.await.unwrap() }
        })
    }
}

fn register_methods(builder: &ClassBuilder<FileTreeHandler>) {
    builder
        .method("build_file_tree", Async::new(BuildFileTreeFn))
        .done();

    builder
        .method("filter_file_tree", Async::new(FilterFileTreeFn))
        .done();
}
