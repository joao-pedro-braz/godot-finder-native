use std::borrow::Cow;
use std::cmp::min;
use std::collections::HashMap;
use std::path::Path;

use crate::script_parser::ScriptProperty;

const FILE_THRESHOLD: f32 = 0.6;
const PROPERTY_THRESHOLD: f32 = 0.6;

const FILE_WHOLE_PATH_SUB_STRING_SCORE: f32 = 0.2;
const FILE_NAME_SUB_STRING_SCORE: f32 = 0.1;
const PROPERTY_SUB_STRING_SCORE: f32 = 0.05;
const STRICT_MATCH_CHAR: &str = "!";

pub fn classify_file(file_path: &str, search: &str) -> f32 {
    let mut search_fragments: Vec<_> = search.split(" ").collect();

    if search_fragments.last().unwrap().is_empty() {
        search_fragments.pop();
    }

    let search_fragments_size = search_fragments.len();
    let mut valid_fragments = 0;
    let mut lowest_score = 1.0_f32;
    for mut search_fragment in search_fragments {
        let strict_mode = search_fragment.starts_with(STRICT_MATCH_CHAR);
        if strict_mode {
            search_fragment = &search_fragment[1..];
        }

        let score = match_file_path(file_path, search_fragment, strict_mode);
        if score <= FILE_THRESHOLD {
            lowest_score = score.min(lowest_score);
            valid_fragments += 1
        }
    }

    return if valid_fragments == search_fragments_size {
        lowest_score
    } else {
        1.0
    };
}

pub fn classify_script_properties(
    properties: Vec<ScriptProperty>,
    search: &str,
) -> (HashMap<ScriptProperty, f32>, f32) {
    let mut search_fragments: Vec<_> = search.split(" ").collect();

    if search_fragments.last().unwrap().is_empty() {
        search_fragments.pop();
    }

    let mut min_score: f32 = 1.0;
    let mut scores: HashMap<ScriptProperty, f32> = HashMap::new();
    let mut valid_fragments = 0;
    for mut search_fragment in search_fragments {
        let strict_mode = search_fragment.starts_with(STRICT_MATCH_CHAR);
        if strict_mode {
            search_fragment = &search_fragment[1..];
        }

        let normalized_search = search_fragment.replace("_", "");
        for property in properties.clone() {
            let score = if property.name.contains(search_fragment) {
                PROPERTY_SUB_STRING_SCORE
            } else {
                check_for_underscore_abbreviation(
                    property.name.as_str(),
                    normalized_search.as_str(),
                )
                .min(
                    check_for_camel_case_abbreviation(
                        property.name.as_str(),
                        normalized_search.as_str(),
                    )
                    .min(levenshtein_distance(
                        property.name.as_str(),
                        search_fragment,
                        strict_mode,
                    )),
                )
            };

            if score <= PROPERTY_THRESHOLD {
                min_score = if score < min_score { score } else { min_score };

                scores.insert(property, score);
                valid_fragments += 1
            }
        }
    }

    return (
        if valid_fragments > 0 {
            scores
        } else {
            HashMap::new()
        },
        min_score,
    );
}

pub fn match_file_path(file_path: &str, search: &str, strict_mode: bool) -> f32 {
    if search.contains("/") || search.contains("\\") {
        // Searching for a path
        return if file_path.contains(search) {
            0.0
        } else {
            levenshtein_distance(file_path, search, strict_mode)
        };
    }

    let file_name = Path::new(file_path).file_stem().unwrap().to_str().unwrap();
    let file_full_name = Path::new(file_path).file_name().unwrap().to_str().unwrap();

    let underscore_abbreviation_score = check_for_underscore_abbreviation(file_name, search);
    if underscore_abbreviation_score < 1.0 {
        return underscore_abbreviation_score;
    }

    let camel_case_abbreviation_score = check_for_camel_case_abbreviation(file_name, search);
    if camel_case_abbreviation_score < 1.0 {
        return camel_case_abbreviation_score;
    }

    let file_whole_path_substring_score = if file_path.contains(search) {
        FILE_WHOLE_PATH_SUB_STRING_SCORE
    } else {
        1.0
    };

    let file_name_substring_score = if file_full_name.contains(search) {
        FILE_NAME_SUB_STRING_SCORE
    } else {
        1.0
    };

    if file_whole_path_substring_score < 1.0 || file_name_substring_score < 1.0 {
        return file_whole_path_substring_score.min(file_name_substring_score);
    }

    let name_score = levenshtein_distance(file_full_name, search, strict_mode);
    let whole_path_score = levenshtein_distance(file_path, search, strict_mode);

    if name_score < 1.0 {
        name_score + 0.01
    } else {
        name_score
    }
    .min(if whole_path_score < 1.0 {
        whole_path_score + 0.01
    } else {
        whole_path_score
    })
}

pub fn levenshtein_distance(this: &str, that: &str, strict_mode: bool) -> f32 {
    // If strict matching, no levenshtein distance
    if strict_mode {
        return 1.0;
    }

    let mut _this = this;
    let mut _that = that;

    if _this.len() > _that.len() {
        _this = that;
        _that = this;
    }

    let mut distances: Vec<_> = (0..(_this.len() + 1)).collect();
    let mut that_index: usize = 0;
    for that_char in _that.chars() {
        let mut new_distances = vec![that_index + 1];

        let mut this_index: usize = 0;
        for this_char in _this.chars() {
            if this_char == that_char {
                new_distances.push(distances[this_index]);
            } else {
                new_distances.push(
                    1 + min(
                        min(distances[this_index], distances[this_index + 1]),
                        *new_distances.last().unwrap(),
                    ),
                );
                this_index += 1
            }
        }

        distances = new_distances;
        that_index += 1;
    }

    *distances.last().unwrap() as f32 / _that.len() as f32
}

pub fn check_for_camel_case_abbreviation(text: &str, abbreviation: &str) -> f32 {
    // start the builder list with the first character
    // enforce upper case
    let mut builder_list: Vec<Cow<str>> = Vec::new();
    for piece in text.chars() {
        if builder_list.is_empty() {
            builder_list.push(Cow::from(piece.to_string()));
            continue;
        }

        // get the last character in the last element in the builder
        let previous_character = builder_list.last().unwrap().chars().last().unwrap();
        if previous_character.to_lowercase().to_string() == previous_character.to_string()
            && piece.to_lowercase().to_string() == piece.to_string()
        {
            // start a new element in the list
            builder_list.push(Cow::from(piece.to_string()))
        } else {
            // append the character to the last string
            builder_list.last_mut().unwrap().to_mut().push(piece)
        }
    }

    check_for_abbreviation(builder_list, abbreviation)
}

pub fn check_for_underscore_abbreviation(text: &str, abbreviation: &str) -> f32 {
    check_for_abbreviation(text.split("_").map(Cow::from).collect(), abbreviation)
}

pub fn check_for_abbreviation(pieces: Vec<Cow<str>>, abbreviation: &str) -> f32 {
    let mut idx: usize = 0;

    let abbreviation_chars: Vec<_> = abbreviation.chars().collect();
    for piece in pieces {
        if piece.is_empty() {
            continue;
        }

        if idx == abbreviation.len() {
            return 0.0;
        }

        if let Some(char) = piece.chars().next() {
            if char == abbreviation_chars[idx] {
                idx += 1
            } else {
                break;
            }
        }
    }

    if idx == abbreviation.len() {
        return 0.0;
    }

    1.0
}
