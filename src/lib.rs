use gdnative::prelude::*;

mod async_executor_driver;
mod file_tree;
mod file_tree_handler;
mod fuzzy_matching;
mod helpers;
mod script_parser;
mod shared_local_pool;

thread_local! {
    static EXECUTOR: &'static shared_local_pool::SharedLocalPool = {
        Box::leak(Box::new(shared_local_pool::SharedLocalPool::default()))
    };
}

fn init(handle: InitHandle) {
    gdnative::tasks::register_runtime(&handle);
    gdnative::tasks::set_executor(EXECUTOR.with(|e| *e));

    async_executor_driver::init(&handle);
    file_tree_handler::init(&handle);
    file_tree::init(&handle);
}

godot_init!(init);
