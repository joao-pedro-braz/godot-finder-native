use std::collections::HashMap;

use bincode::{Decode, Encode};
use gdnative::prelude::*;
use lazy_static::lazy_static;
use regex::Regex;

lazy_static! {
    static ref SIGNAL_EXPR_PATTERN: Regex = Regex::new(r"^signal\s*([\w]+)").unwrap();
    static ref NAMED_ENUM_EXPR_PATTERN: Regex = Regex::new(r"^enum\s*([\w]+)").unwrap();
    static ref CONSTANT_EXPR_PATTERN: Regex = Regex::new(r"^const\s*([\w]+)").unwrap();
    static ref VARIABLE_EXPR_PATTERN: Regex =
        Regex::new(r"^(var|onready\s*var|export\s*var|export\(.*\)\s*var)\s*([\w]+)").unwrap();
    static ref FUNCTION_EXPR_PATTERN: Regex =
        Regex::new(r"(^func|^static\s*func)\s*([\w]+)").unwrap();
}

#[derive(PartialEq, Eq, Debug, Default, ToVariant, FromVariant, Encode, Decode)]
pub struct Script {
    signals: HashMap<String, ScriptProperty>,
    named_enums: HashMap<String, ScriptProperty>,
    constants: HashMap<String, ScriptProperty>,
    variables: HashMap<String, ScriptProperty>,
    functions: HashMap<String, ScriptProperty>,
}

#[derive(PartialEq, Eq, Debug, Hash, Clone, ToVariant, FromVariant, Encode, Decode)]
pub struct ScriptProperty {
    pub name: String,
    pub row: i32,
    pub column: i32,
    pub property_type: String,
}

impl Script {
    pub fn new() -> Self {
        Script::default()
    }

    pub fn enumerate(&self) -> Vec<ScriptProperty> {
        let signals: Vec<_> = self.signals.values().collect();
        let named_enums: Vec<_> = self.named_enums.values().collect();
        let constants: Vec<_> = self.constants.values().collect();
        let variables: Vec<_> = self.variables.values().collect();
        let functions: Vec<_> = self.functions.values().collect();

        [signals, named_enums, constants, variables, functions]
            .concat()
            .iter()
            .map(|property| (*property).clone())
            .collect()
    }

    pub fn parse_script(&mut self, script_content: String) {
        let mut idx: i32 = -1;
        for line in script_content.lines() {
            idx += 1;

            if self.parse_signal(idx, line).is_some() {
                continue;
            }

            if self.parse_named_enum(idx, line).is_some() {
                continue;
            }

            if self.parse_constant(idx, line).is_some() {
                continue;
            }

            if self.parse_variable(idx, line).is_some() {
                continue;
            }

            if self.parse_function(idx, line).is_some() {
                continue;
            }
        }
    }

    fn parse_signal(&mut self, idx: i32, line: &str) -> Option<()> {
        let captures = SIGNAL_EXPR_PATTERN.captures(line)?;
        let name = captures.get(2)?.as_str().to_string();

        self.signals.insert(
            name.clone(),
            ScriptProperty {
                name,
                row: idx,
                column: captures.get(2)?.start() as i32,
                property_type: "signal".to_string(),
            },
        );

        Some(())
    }

    fn parse_named_enum(&mut self, idx: i32, line: &str) -> Option<()> {
        let captures = NAMED_ENUM_EXPR_PATTERN.captures(line)?;
        let name = captures.get(2)?.as_str().to_string();

        self.named_enums.insert(
            name.clone(),
            ScriptProperty {
                name,
                row: idx,
                column: captures.get(2)?.start() as i32,
                property_type: "named_enum".to_string(),
            },
        );

        Some(())
    }

    fn parse_constant(&mut self, idx: i32, line: &str) -> Option<()> {
        let captures = CONSTANT_EXPR_PATTERN.captures(line)?;
        let name = captures.get(2)?.as_str().to_string();

        self.constants.insert(
            name.clone(),
            ScriptProperty {
                name,
                row: idx,
                column: captures.get(2)?.start() as i32,
                property_type: "constant".to_string(),
            },
        );

        Some(())
    }

    fn parse_variable(&mut self, idx: i32, line: &str) -> Option<()> {
        let captures = VARIABLE_EXPR_PATTERN.captures(line)?;
        let name = captures.get(2)?.as_str().to_string();

        self.variables.insert(
            name.clone(),
            ScriptProperty {
                name,
                row: idx,
                column: captures.get(2)?.start() as i32,
                property_type: "variable".to_string(),
            },
        );

        Some(())
    }

    fn parse_function(&mut self, idx: i32, line: &str) -> Option<()> {
        let captures = FUNCTION_EXPR_PATTERN.captures(line)?;
        let name = captures.get(2)?.as_str().to_string();

        self.functions.insert(
            name.clone(),
            ScriptProperty {
                name,
                row: idx,
                column: captures.get(2)?.start() as i32,
                property_type: "function".to_string(),
            },
        );

        Some(())
    }
}
