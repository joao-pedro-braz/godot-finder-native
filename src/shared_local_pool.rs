use std::ops::Deref;

use tokio::task::LocalSet;

#[derive(Default)]
pub struct SharedLocalPool {
    local_set: LocalSet,
}

impl futures::task::LocalSpawn for SharedLocalPool {
    fn spawn_local_obj(
        &self,
        future: futures::task::LocalFutureObj<'static, ()>,
    ) -> Result<(), futures::task::SpawnError> {
        self.local_set.spawn_local(future);

        Ok(())
    }
}

impl Deref for SharedLocalPool {
    type Target = LocalSet;

    fn deref(&self) -> &Self::Target {
        &self.local_set
    }
}
